title=Turn ABP on/off manually
description=Here's how you can manually turn off or on Adblock Plus at any time in just a few clicks.
template=article
product_id=abp
category=Customization & Settings

Users can manually turn Adblock Plus on and off at any time, which is especially useful when certain websites ask to turn off ad blockers, or when users want to support their favorite content creators via ad revenue.

<section class="platform-chrome" markdown="1">
## Chrome

1. Click on the Adblock Plus extension icon on the right of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
The *Adblock Plus Settings* tab opens.
1. There are two toggles. The first toggle allows you to turn Adblock Plus on or off for the entire website you are currently visiting.
1. The second toggle turns Adblock Plus on or off just for the specific webpage you are currently visiting.
1. The page should be refreshed for the changes to take effect.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Click on the Adblock Plus extension icon on the right of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
The *Adblock Plus Settings* tab opens.
1. There are two toggles. The first toggle allows you to turn Adblock Plus on or off for the entire website you are currently visiting.
1. The second toggle turns Adblock Plus on or off just for the specific webpage you are currently visiting.
1. The page should be refreshed for the changes to take effect.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. Click on the Adblock Plus extension icon on the right of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
The *Adblock Plus Settings* tab opens.
1. There are two toggles. The first toggle allows you to turn Adblock Plus on or off for the entire website you are currently visiting.
1. The second toggle turns Adblock Plus on or off just for the specific webpage you are currently visiting.
1. The page should be refreshed for the changes to take effect.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-opera" markdown="1">
## Opera

1. Click on the Adblock Plus extension icon on the right of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
The *Adblock Plus Settings* tab opens.
1. There are two toggles. The first toggle allows you to turn Adblock Plus on or off for the entire website you are currently visiting.
1. The second toggle turns Adblock Plus on or off just for the specific webpage you are currently visiting.
1. The page should be refreshed for the changes to take effect.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Click on the Adblock Plus extension icon on the right of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
The *Adblock Plus Settings* tab opens.
1. There are two toggles. The first toggle allows you to turn Adblock Plus on or off for the entire website you are currently visiting.
1. The second toggle turns Adblock Plus on or off just for the specific webpage you are currently visiting.
1. The page should be refreshed for the changes to take effect.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-ios" markdown="1">
## iOS Safari

1. Open up the Adblock Plus for iOS app on your device.
1. Tap the toggle next to Block ads to turn Adblock Plus on or off.
When the toggle is red, Adblock Plus is turned on.
1. Close out of the app and continue browsing Safari.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-safari" markdown="1">
## Safari

1. Click on the Adblock Plus extension icon on the left of the browser’s toolbar.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon).
</aside>
1. Unselect the checkbox to turn off Adblock Plus for the entire website you are currently on.
The page will automatically refresh for the change to take effect.
1. To turn Adblock Plus on again, select the checkbox.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. Click on the gear icon in the top right corner of the browser’s toolbar and select Manage add-ons.
1. In the Manage add-ons window that opens up, find and select Adblock Plus.
1. Click Disable on the bottom right to turn off Adblock Plus.
1. To turn Adblock Plus on again repeat steps 1-2 and then click Enable on the bottom right of the Manage add-ons window.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>

<section class="platform-samsungBrowser" markdown="1">
## Samsung Internet

1. Open Samsung Internet.
1. Tap the icon in the bottom right corner of the screen.
1. Tap Ad blockers.
1. Find Adblock Plus under Installed.
1. Tap the toggle to the left to turn Adblock Plus on or off.
When Adblock Plus is turned on the toggle is blue.

Keep in mind that Adblock Plus will remain turned off for that webpage or website when you visit it again. So if you’re [still seeing intrusive ads](adblockplus/i-still-see-ads), check that Adblock Plus has been turned back on.

Users can also turn off Adblock Plus for certain websites and/or support content creators by [whitelisting the website](adblockplus/add-a-website-to-the-whitelist).
</section>