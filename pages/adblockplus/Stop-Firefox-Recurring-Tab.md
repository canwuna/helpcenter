title=Stop recurring tab in Firefox
description=Some Firefox users might be experiencing an issue where a new successful installation tab appears over and over.
template=article
product_id=abp
category=Troubleshooting & Reporting

Some Firefox users have reported seeing a recurring new tab stating that installation of ABP is successful. This is a known issue and we are working with the developers at Mozilla to correct this. In the meantime, users have found success by uninstalling and reinstalling Adblock Plus via [AdblockPlus.org](https://adblockplus.org). 

If a reinstall does not correct the issue, try creating a new Firefox profile. You can create a new Firefox profile directly from the browser by folowing the steps below.

1. Enter `about:profiles` into the browser's URL search bar.
2. On the page, click **Create a New Profile**.
3. Read the introduction and click **Next** / **Continue**.
4. Enter a name for your new profile.
<aside class="alert info" markdown="1">
**Tip**: Use a profile name that is descriptive, such as your personal name. This name is not exposed to the Internet. Optionally, to change where the profile is stored on your computer, click **Choose Folder**.
</aside>
5. Click **Finish** / **Done**.

For more information on managing profiles in Firefox, refer to this [Mozilla support article](https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Multiple_profiles).