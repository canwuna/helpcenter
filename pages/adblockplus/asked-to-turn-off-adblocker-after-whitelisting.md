title=Why am I still asked to turn off my ad blocker after whitelisting?
description=Are you still being asked to turn off your ad blocker after disabling or adding the website to your whitelist? See how you can troubleshoot this.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

Are you still being asked to turn off your ad blocker after disabling or adding the website to your whitelist? Often times this happens when you have other security features enabled on your browser. ie: Tracking protection on Firefox. [Firefox's Tracking Protection and whitelisting](adblockplus/firefox-tracking-protection-whitelisting)

**Suggested troubleshooting steps: **

1. Clear your browser's cache and cookies. [How to clear cache and cookies](adblockplus/how-to-clear-cache-and-cookies)
2. If you have other extensions installed, try disabling them to make sure they aren't causing the issue. [How to check your installed extensions](adblockplus/how-to-check-your-installed-extensions) 
3. Also check your browser's installed security settings. [How to check your browser security settings](adblockplus/how-to-check-your-browser-settings)

Check for settings or extensions that block javascript, tracking, cookies and of course ads/pop ups.