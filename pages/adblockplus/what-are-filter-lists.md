title=What are filter lists?
description=Learn more about what Adblock Plus filter lists are and what you can do with them.
template=article
product_id=abp
category=Troubleshooting & Reporting

Filter lists are sets of rules that tell your browser which elements to block. You can block as little or as much as you want. Choose from pre-made, externally maintained filter lists, or [create your own (English only)](adblockplus/how-to-write-filters). Almost all pre-made filter lists are created, published and maintained by users for users under open source licenses.

## Default filter lists

The following filter lists come pre-installed with Adblock Plus:

* Acceptable Ads
* EasyList (+ bundled language filter list - depending on your browser’s language setting)
* ABP Anti-circumvention filter list

## Where to Find Adblock Plus Filter Lists

<section class="platform-chrome" markdown="1">
### Chrome

1. Click on the **ABP icon**.
1. Click on the **gear icon**.
1. Go to **Advanced**, located on the left of your page.
1. Scroll down to Filter Lists.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. Click on the **ABP icon**.
1. Click on the **gear icon**.
1. Go to **Advanced**, located on the left of your page.
1. Scroll down to Filter Lists.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. Click on the **ABP icon**.
1. Click on the **gear icon**.
1. Go to **Advanced**, located on the left of your page.
1. Scroll down to Filter Lists.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. Click on the **ABP icon**.
1. Click on the **gear icon**.
1. Go to **Advanced**, located on the left of your page.
1. Scroll down to Filter Lists.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. Click on the **ABP icon**.
1. Click on the **gear icon**.
1. Go to **Advanced**, located on the left of your page.
1. Scroll down to Filter Lists.
</section>

<section class="platform-safari" markdown="1">
### Safari

Due to changes on Apple regulations, custom filter lists are no longer supported on Safari.
</section>

<section class="platform-samsungBrowser" markdown="1">
### Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
1. Tap **Configure your filter lists**.
</section>

## How to subscribe to a filter list

You can see how to subscribe to a filter list in this article: [Add a new filter list](adblockplus/add-a-filter-list)

<aside class="alert info" markdown="1">
**Important:** It is not recommended to add too many filter lists to Adblock Plus. This can slow down the ad blocker and, therefore, your browsing.
</aside>