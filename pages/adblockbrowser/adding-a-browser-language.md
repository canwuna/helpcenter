title=How to add your browsing language
description=Add a preferred language to Adblock Browser for Android.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Scroll to the **Advanced** section.
4. Tap on **Languages**.
5. Tap **Add language** and select the language you wish to browse in.
<br>The new language is added to your list of preferred languages.

![Change your preferred language in Adblock Browser](/src/img/gif/abb-browser-language.gif)