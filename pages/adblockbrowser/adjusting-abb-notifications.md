title=Adjusting Adblock Browser notifications
description=Set your Adblock Browser for Android notification preferences.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

Notifications can be very helpful, but we also understand that users want to customize how often they see notifications and from who. Follow the instructions below to adjust your notifications settings to your liking. 

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Notifications**.
4. Toggle the notification settings to best suit your preferences.

![Adjust your notification settings in Adblock Browser](/src/img/gif/abb-notifications.gif)
