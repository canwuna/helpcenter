title=Adding a website to your allowlist
description=Add websites that you trust and want to support to your Adblock Browser allowlist. Ads will be shown on these websites.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

Not able to view content on a website with ABP enabled? Or do you want to support a particular site by allowing ads? Add the URL to your allowlist using the instructions below.

<aside class="alert info" markdown="1">
**Important**: All ads, including those that do not adhere to the Acceptable Ads criteria, will be shown on whitelisted websites.
</aside>

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **Allowlist**.
5. Enter the website URL and tap the **+** icon.

![Add a website to your allowlist in Adblock Browser](/src/img/gif/add-to-allowlist-abb.gif)